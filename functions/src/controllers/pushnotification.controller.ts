
const FCM = require('fcm-push'); 
const serverKey = 'AAAAVxXznak:APA91bF0zBPR_yfPIo29huOQcNKD54mfJ9tNcc7qgqoZ4N_BiEbVEhe619gwouL8Zmxbm1E2Y_kbL1DPB2_62nXRSm3BROM1UUDUPIKvHEuwktUP_KigQ_C78CwPrCxmqFh9-fIUC2ifANqW-j4psuhR8UZrOXU8vA';
const fcm = new FCM(serverKey);

class PushnotificationController {

    public sendRejectNotification(request, response, next){
        const token=request.query.token;
        const message = {
            to: token, // required fill with device token or topics
           
            notification: {
                title: 'Title of your push notification',
                body: 'Body of your push notification'
            }
        };
        
        //callback style
        fcm.send(message, function(err, res){
            if (err) {
                response.send(500, err);
                // console.log("Something has gone wrong!");
            } else {
                response.send(200,res);
                // console.log("Successfully sent with response: ", response);
            }
        });
    }

}
export default new PushnotificationController();