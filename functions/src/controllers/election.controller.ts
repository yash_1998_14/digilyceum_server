import electionService from "../services/election.service";
import * as functions from 'firebase-functions';
import { resolve } from "path";
const nodemailer = require('nodemailer');
// const twilio = require('twilio');
const SendOtp = require('sendotp');
// const sendOtp = new SendOtp('240903ApMGc7Lo5bd8af01');
const sendOtp = new SendOtp('270013AgDkIbPGwW5c9eb813');
sendOtp.setOtpExpiry('1');




class CredentialController {
    public sendOtpForUserNameLogin = (request, response, next) => {
        const phoneNumber = request.query.number;
        sendOtp.send("91" + phoneNumber, "Digilyceum", function (error, data) {
            response.send(200, data);
        });
    }

    public sendOtp = (request, response, next) => {
        const electionCode = request.query.electionCode;
        console.log(electionCode);
        electionService.getCurrentElectionData(electionCode).then((resp) => {
            const electionData = resp;
            const randomcode = Math.floor(Math.random() * 899999 + 100000);
            Object.keys(electionData['users']).map(function (key, index) {
                console.log(electionData['users'][key].phone_no);
                console.log(electionData['users'][key].email);
                sendOtp.send("91" + electionData['users'][key].phone_no, "Digilyceum", function (error, data) {
                    response.send(200, data);
                });
            });
        }).catch((error) => {
            response.send(500, error);
        })
    }

    public verifyOtp = (request, response, next) => {
        const number = request.query.number;
        const otp = request.query.otp;
        sendOtp.verify("91" + number, otp, function (error, data) {
            console.log(data); // data object with keys 'message' and 'type'
            if (data.type == 'success') {
                response.send(200, 'OTP verified successfully');
            }
            if (data.type == 'error') {
                response.send(500, 'OTP verification failed');
            }
        });
    }

    public resendOtp = (request, response, next) => {
        const number = request.query.number;
        sendOtp.retry("91" + number, false, function (error, data) {
            response.send(200, data);
        });
    }
}

export default new CredentialController();