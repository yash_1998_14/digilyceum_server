import userservice from "../services/user.service";
class UserController {
  public login = (req, res, next) => {
    const enrollmentNumber = req.query.enrollmentNumber;
    const password = req.query.password;
    userservice
      .getUserInformation(enrollmentNumber)
      .then((data: any) => {
        res.send(data);
      })
      .catch(error => {
        res.send(500, error);
      });
  };

  public sendUserNameEmail = (req, res, next) => {
      const email= req.query.email; 
      const username= req.query.username;
      userservice.sendEmail(email,username).then((data: any) => {
        res.send(data);
      })
      .catch(error => {
        res.send(500, error);
      });
  };
}

export default new UserController();
