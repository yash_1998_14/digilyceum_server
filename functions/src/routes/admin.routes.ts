import * as express from "express";
import electionCtrl from "../controllers/election.controller";
import userCtrl from "../controllers/User.controller";
import pushnotificationCtrl from '../controllers/pushnotification.controller';
const router = express.Router();

router.route('/sendOtp').get(electionCtrl.sendOtp);
router.route('/login').get(userCtrl.login);
router.route('/sendOtpForUserNameLogin').get(electionCtrl.sendOtpForUserNameLogin);
router.route('/verifyOtp').get(electionCtrl.verifyOtp);
router.route('/resendOtp').get(electionCtrl.resendOtp);
router.route('/sendRejectNotification').get(pushnotificationCtrl.sendRejectNotification);
router.route('/sendUserNameEmail').get(userCtrl.sendUserNameEmail);

export default router;