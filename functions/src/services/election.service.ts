import fbService from './firebase.service';

class ElectionService {

    public getCurrentElectionData(game_code) {
        return new Promise((resolve, reject) => {
            const data = fbService.firebase.ref("games/" + game_code);
            data.on("value", function (snapshot) {
                console.log(snapshot);
                if (snapshot.exists()) {
                    resolve(snapshot.val());
                    console.log(snapshot.val());
                } else {
                    console.log("Cannot read a TODO item with TODO ID %s that does not exist.", game_code);
                    return null;
                }
            }, function (errorObject) {
                console.log("readToDoItem failed: " + errorObject.code);
                reject(errorObject.code);
            });

        });
    }

    public setOtpOnUser(game_code,player_id,otp){
        const pathParams = {
            game_code: game_code,
            player_id: player_id
          };
          const updateInfo = {
            OtpVerification:otp
          };
          return fbService.update('games/$game_code/users/$player_id', updateInfo, pathParams);
        
        // return new Promise((resolve, reject) => {
        //     const data = fbService.firebase.ref("games/$game_code/users/$player_id");
        // });
    }


}

export default new ElectionService();