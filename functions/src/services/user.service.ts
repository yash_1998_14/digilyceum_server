import fbService from './firebase.service';
const nodemailer = require('nodemailer');
class UserService {
    public login = (email, password) => {
        return new Promise((res, rej) => {
            fbService.auth.signInWithEmailAndPassword(email, password)
                .then(credential => this.createToken(credential.user.uid))
                .then(token => res(token))
                .catch(err => rej(err));
        });
    }
    public createToken = (uid) => {
        //Additional fields that we can add to the token
        let additionalFields = {}

        return fbService.adminAuth.createCustomToken(uid, additionalFields);
    }
    public verifyToken = (token) => {
        return new Promise((res, rej) => {
            fbService.adminAuth.verifyIdToken(token)
                .then((decodedToken) => this.createToken(decodedToken.uid))
                .then((loginToken) => fbService.auth.signInWithCustomToken(loginToken))
                .then(credential => res(credential))
                .catch(err => rej(err))
        })
    }
    public signUp = (form, phoneAuth) => {
        return new Promise((res, rej) => {
            let user: any;
            let credentials = { email: form.email, password: form.password };
            delete form['password'];
            delete form['confirmPassword'];

            fbService.auth.createUserWithEmailAndPassword(credentials.email, credentials.password)
                .then((creds) => fbService.adminAuth.updateUser(creds.user.uid, { displayName: `${form.firstName} ${form.lastName}` }))
                .then((userF) => {
                    user = userF;
                    return fbService.db.collection('users').doc(user.uid).set(form);
                })
                .then(() => fbService.adminAuth.deleteUser(phoneAuth.uid))
                .then(() => this.createToken(user.uid))
                .then((token) => res(token))
                .catch((err) => rej(err))
        })
    }

    public getUserDetails(userId) {
        return new Promise((resolve, reject) => {
            const data = fbService.db.ref("users/" + userId);
            data.on("value", function (snapshot) {
                if (snapshot.exists()) {
                    resolve(snapshot.val());
                    console.log(snapshot.val());
                } else {
                    console.log("Cannot read a TODO item with TODO ID %s that does not exist.", userId);
                    return null;
                }
            }, function (errorObject) {
                console.log("readToDoItem failed: " + errorObject.code);
                reject(errorObject.code);
            });
        });
    }

    public getUserInformation(enrollmentNumber) {
        return new Promise((resolve, reject) => {
            const data = fbService.db.ref("student_information/" + enrollmentNumber);
            data.on("value", function (snapshot) {
                if (snapshot.exists()) {
                    resolve(snapshot.val());
                    console.log(snapshot.val());
                } else {
                    console.log("Cannot read a TODO item with TODO ID %s that does not exist.", enrollmentNumber);
                    return null;
                }
            }, function (errorObject) {
                console.log("readToDoItem failed: " + errorObject.code);
                reject(errorObject.code);
            });
        });
    }

    public sendEmail(email,username) {
        return new Promise((resolve, reject) => {
            const APP_NAME = "Digilyceum";
            const gmailEmail = "yashpanwala90@gmail.com";
            const gmailPassword = "Y@sh1234567";
            const mailTransport = nodemailer.createTransport({
                service: 'gmail',
                auth: {
                    user: gmailEmail,
                    pass: gmailPassword
                }
            });
            const mailOptions = {
                from: `${APP_NAME} <noreply@gmail.com>`,
                to: email,
                subject: 'UserName',
                html: `<p>You asign as a subadmin and your username is ${username}.<br>Please use this username for login<br>Thanks</p>`
            };
            // The user subscribed to the newsletter.
            mailTransport.sendMail(mailOptions).then((data) => {
                resolve(data);
            });
        });
    }

}

export default new UserService();