import * as express from "express";
import * as bodyParser from "body-parser";
import routes from "../routes/admin.routes";
const cookieParser = require('cookie-parser')();
const cors = require('cors')({ origin: true });
class App {

    public app: express.Application;

    constructor() {
        this.app = express();
        this.config();
    }

    private config(): void {
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: true }));

        this.app.use(cors);
        this.app.use(cookieParser);

        // Emulate the body of the methods POST and PUT, when it's a GET or delete
        this.app.use((req, res, next) => {
            // Allow Access control for other origins
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Authorization, Accept");
            res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS");
            if ('OPTIONS' === req.method) res.send(200);
            else {
                if ('GET' === req.method || "DELETE" === req.method) req.body = req.query;
                next();
            }
        });

        //Import all the routes from the router file
        this.app.use('/', routes);

        //The 404 Route (ALWAYS Keep this as the last route)
        this.app.all('*', function (req, res) {
            res.status(404).json({ 'msg': 'Url not found' });
        });
        
        // Error handler middleware
        this.app.use((err, req, res, next) => {
            const msg = err.msg || err;
            const status = err.status || 500;
            const errorName = err.name || "Error";

            // Setting error to log on stream.
            req.error = errorName + ": " + JSON.stringify(msg);

            // HTTP Response
            res.status(status).json({ 'msg': msg })
        });
    }

}
export default new App().app;